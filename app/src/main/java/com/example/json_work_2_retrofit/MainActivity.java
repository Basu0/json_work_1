package com.example.json_work_2_retrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
 TextView textView;
 private JsonPlaseHolderAPI jsonPlaseHolderAPI;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView=(TextView)findViewById(R.id.Text_viw_result);

        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
         jsonPlaseHolderAPI=retrofit.create(JsonPlaseHolderAPI.class);

       // getpost();
        //getComment();
       // Createpost();
        updatepost();


    }

    private void updatepost() {

        Post post=new Post(12,"null","Name");
        Call<Post> call=jsonPlaseHolderAPI.patchpost(5,post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (!response.isSuccessful()){

                    textView.setText("Code :"+response.code());
                    return;
                }
                Post postrespons=response.body();
                String Contant = "";
                Contant +="Code "+response.code() + "\n";
                Contant+="ID : "+postrespons.getId()+ "\n";
                Contant+="UserId : "+postrespons.getUserId()+ "\n";
                Contant+="Titel : "+postrespons.getTitle()+ "\n";
                Contant+="Text : "+postrespons.getText()+ "\n";
                textView.append(Contant);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }

    private void Createpost() {

        Post post=new Post(23,"New Name","HEllo");

        Call<Post> call=jsonPlaseHolderAPI.createpos(post);
        call.enqueue(new Callback<Post>() {
            @Override
            public void onResponse(Call<Post> call, Response<Post> response) {
                if (!response.isSuccessful()){

                    textView.setText("Code :"+response.code());
                    return;
                }
                Post postrespons=response.body();
                String Contant = "";
                Contant +="Code "+response.code() + "\n";
                Contant+="ID : "+postrespons.getId()+ "\n";
                Contant+="UserId : "+postrespons.getUserId()+ "\n";
                Contant+="Titel : "+postrespons.getTitle()+ "\n";
                Contant+="Text : "+postrespons.getText()+ "\n";
                textView.append(Contant);
            }

            @Override
            public void onFailure(Call<Post> call, Throwable t) {

            }
        });
    }

    private void getComment() {

        Call<List<Comment>> call=jsonPlaseHolderAPI.getComment(4);
        call.enqueue(new Callback<List<Comment>>() {
            @Override
            public void onResponse(Call<List<Comment>> call, Response<List<Comment>> response) {
                if (!response.isSuccessful()){

                    textView.setText("Code :"+response.code());
                    return;
                }

                List<Comment> posts=response.body();
                for (Comment post:posts){

                    String Contant = "";
                    Contant+="postId : "+post.getPostId()+ "\n";
                    Contant+="ID : "+post.getId()+ "\n";
                    Contant+="Name : "+post.getName()+ "\n";
                    Contant+="Email : "+post.getEmail()+ "\n";
                    Contant+="Text : "+post.getText()+ "\n";
                    textView.append(Contant);

                }

            }

            @Override
            public void onFailure(Call<List<Comment>> call, Throwable t) {
                textView.setText("error"+t.getMessage());

            }
        });

    }

    public void getpost(){
        Map<String, String> parameters = new HashMap<>();
        parameters.put("userId", "1");
        parameters.put("_sort", "id");
        parameters.put("_order", "desc");
      Call<List<Post>> call= (Call<List<Post>>) jsonPlaseHolderAPI.getpost(parameters);
        call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if (!response.isSuccessful()){

                    textView.setText("Code :"+response.code());
                    return;
                }

                List<Post> posts=response.body();
                for (Post post:posts){

                    String Contant = "";
                    Contant+="ID : "+post.getId()+ "\n";
                    Contant+="UserId : "+post.getUserId()+ "\n";
                    Contant+="Titel : "+post.getTitle()+ "\n";
                    Contant+="Text : "+post.getText()+ "\n";
                    textView.append(Contant);

                }

            }

            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {
                textView.setText("error"+t.getMessage());

            }
        });
    }

}
