package com.example.json_work_2_retrofit;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface JsonPlaseHolderAPI {
    @GET("posts")
   Call<List<Post>> getpost( @Query("userId") Integer[] userId,
                             @Query("_sort") String sort,
                             @Query("_order") String order );

    @GET("posts")
    Call<List<Post>> getpost(@QueryMap Map<String, String> parameters);

    @GET("posts/{id}/comments")
    Call<List<Comment>> getComment(@Path("id") int postId);

   @POST("posts")
    Call<Post> createpos(@Body Post post);

   @PUT("posts/{id}")
    Call<Post>putpost(@Path("id") int id,@Body Post post);

    @PATCH("posts/{id}")
    Call<Post>patchpost(@Path("id") int id,@Body Post post);





}
